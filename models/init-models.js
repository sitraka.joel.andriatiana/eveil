var DataTypes = require("sequelize").DataTypes;
var _AccessVideo = require("./AccessVideo");
var _Module = require("./Module");
var _Module_AccessVideo = require("./Module_AccessVideo");

function initModels(sequelize) {
  var AccessVideo = _AccessVideo(sequelize, DataTypes);
  var Module = _Module(sequelize, DataTypes);
  var Module_AccessVideo = _Module_AccessVideo(sequelize, DataTypes);

  Module_AccessVideo.belongsTo(AccessVideo, { foreignKey: "AccessVideo_Id"});
  AccessVideo.hasMany(Module_AccessVideo, { foreignKey: "AccessVideo_Id"});
  Module_AccessVideo.belongsTo(Module, { foreignKey: "Module_Id"});
  Module.hasMany(Module_AccessVideo, { foreignKey: "Module_Id"});

  return {
    AccessVideo,
    Module,
    Module_AccessVideo,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
