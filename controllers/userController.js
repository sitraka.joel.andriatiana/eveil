const AccessVideo = require('../models').AccessVideo;
const crypto = require('crypto');
const sendMail = require('../services/sendEmailService');
const path = require('path');


// const sendMail = require('../services/sendEmailService');

async function generateUrl(user, req, type = 'regenerateMdp') {
    let newUrl = crypto.randomBytes(32).toString('hex');
    return user.update({
        urlMdp: newUrl,
    }).then(async (user) => {
        // send email
        // if user == valide => reset Password
        // if user != valide => new Password
        newUrl = req.protocol + '://' + req.get('host') + '/resetPassword/' + newUrl;
        let subject;
        if (type === 'resetPassword') {
            subject = 'Eveil.mg - Réinitialisation de mot de passe';
        } else {
            subject = 'Eveil.mg – Génération de mot de passe';
        }

        const templateDir = path.join('Mails', 'resetPassword');

        const params = {
            'newUrl': newUrl,
            'user': user
        }
        return sendMail.sendTemplate(params, templateDir, 'enfant.eveil.mg@gmail.com', user.email, subject, null, null)
            .then((info) => {
                return {
                    status: 200,
                    content: info,
                };
            }).catch(error => {
                return {
                    status: 500,
                    content: error,
                };
            });
    }).catch(e => {
        return {
            status: 500,
            content: e,
        };
    });
}

let userController = {
    changePassword(req, res) {
        const urlMdp = req.params.urlMdp;
        console.log(urlMdp);
        return AccessVideo.findOne({
            where: {
                urlMdp: urlMdp,
            },
        }).then((user) => {
            if (!user) {
                return res.redirect('/');
            }
            if (req.method === 'POST') {
                let password = req.body.password;
                let confirmPassword = req.body.confirmPassword;
                if (password !== confirmPassword) {
                    let error = "Les valeurs sont différentes";
                    req.flash('error', error);
                    return res.redirect('back');
                }
                return user.update({
                    password: password,
                    urlMdp: null,
                }).then(() => {
                    // Redirect to login page
                    req.flash('success', 'Mot de passe modifié avec succès!');
                    res.redirect('/login');
                });
            }
            res.render('User/changePassword', {error: req.flash('error')});
        }).catch((err) => res.status(500).send(err));
    },
    resetPassword(req, res) {
        if (req.method === 'POST') {
            const mail = req.body.email;
            const email = mail.toLowerCase();
            return AccessVideo.findOne({
                where: {
                    email: email,
                },
            }).then(async user => {
                if (!user) {
                    console.log('user not found');
                    let error = "Email introuvable";
                    req.flash('error', error);
                    return res.redirect('back');
                }
                let generated = await generateUrl(user, req, 'resetPassword');
                if (generated.status === 200) {
                    // redirect to login?
                    req.flash('success', 'Verifiez votre email!')
                    res.redirect('/');
                } else {
                    res.json(generated);
                }
            }).catch(err => res.status(500).send(err));
        }
        res.render('User/resetPassword', {error: req.flash('error')});
    },
};


module.exports = userController;
