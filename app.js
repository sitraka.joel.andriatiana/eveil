var createError = require('http-errors');
var express = require('express');
var socket_io = require('socket.io');
var path = require('path');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var session = require('express-session');
var morgan = require('morgan');
var logger = require('./services/logger');
var passport = require('passport');

console.log = function() {
  var msgs = [];

  while(arguments.length) {
    let val = [].shift.call(arguments)
    if (typeof val === 'object' && val !== null) val = JSON.stringify(val);
    msgs.push(" " + val);
  }

  logger.console(msgs);
};
// Express
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
// Socket.io
var io = socket_io();
app.io = io;

// Routes
var routes = require('./routes')(io);

// App name
app.locals.app_name = "Eveil";
// Moment.js
var moment = require('moment');
app.locals.moment = require('moment');

// Imports dist from node_modules
app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')));
app.use('/img', express.static(path.join(__dirname, 'assets/img')));
app.use(express.static(path.join(__dirname, 'assets/qrcode')));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// app.use(morgan("dev", {"stream": logger.stream}));
app.use(morgan(':method :url :status - :body :res[content-length] - :response-time ms', {"stream": logger.stream}));
// app.use(morgan("combined", {"stream": logger.stream}));
morgan.token('body', function(req, res) {
  if (req.body) {
    return JSON.stringify(req.body);
  }
});
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({
  secret: 'vanilla pay',
  resave: false,
  saveUninitialized: false,
})); // session secret
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')));
app.use(express.static(path.join(__dirname, 'assets')));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);
//load passport strategies

require('./services/passportAuth')(passport);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
