require('dotenv').config();
module.exports = {
  "period": 5,
  "otpDigit": 7,
  "codeDeblocageDigit": 5,
  "codeDeblocageEmail": "usercoddeblocage@yopmail.com",
  "authorizedUrlPhoto": ["41.207.44.68", "41.207.44.69", "picsum.photos", "via.placeholder.com", "192.168.1.251","mis.fid.mg"],
  "authorizedTypePhoto": ["jpeg", "png", "webp"]
};
