require('dotenv').config();
const {AccessVideo} = require('../models');
const {QueryTypes} = require("sequelize");

module.exports.isLoggedIn = function (req, res, next) {
    if (req.isAuthenticated()) {
        res.locals.user = req.session.userData;
        next();
    } else {
        req.session.originalUrl = req.originalUrl;
        res.redirect('/login');
    }
};
module.exports.setUser = async (req, res, next) => {
    const sql = `SELECT * FROM "AccessVideoModuleView" WHERE "AccessVideo_Id" = '${res.locals.user.id}' AND "dateFin" > now()`;
    const modules = await AccessVideo.sequelize
        .query(sql, {
            type: QueryTypes.SELECT,
        })
        .then(usersRow => {
            /*let modules = usersRow.map(m => {
                return {
                    moduleId: m.Module_Id,
                    moduleLibelle: m.libelle
                }
            });*/
            return usersRow.map(m => m.Module_Id);
        });
    res.locals.user = {...res.locals.user, modules: modules}
    next();
}
module.exports.hasAccess = async (req, res, next) => {
    if (req.params.id) {
        const moduleIdInt = parseInt(req.params.id);
        if (!Number.isInteger(moduleIdInt)) {
            return res.redirect('/');
        }
        const modules = res.locals.user.modules;
        if (modules.includes(moduleIdInt)) {
            next();
        } else {
            return res.redirect('/');
        }
    }
};






