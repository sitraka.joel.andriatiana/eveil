var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var TotpStrategy = require('passport-totp').Strategy;
const AccessVideo = require('../models').AccessVideo;


function verifyAuth(email, password) {
  return AccessVideo.findOne({
    where: {
      email: email.toLowerCase(),
      password: password,
    },
  }).then(async user => {
    if (user) {
      await user.update({
        dateDerniereConn: new Date()
      });
    }
    return user;
  });
}

module.exports = (passport) => {
  passport.use('local-login', new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true, // allows us to pass back the entire request to the callback
    },
    async function (req, email, password, done) {
      var user = await verifyAuth(email, password);
      return done(null, (!user) ? false : user);
    }),
  );


  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    AccessVideo.findByPk(id)
      .then(user => {
        if (!user) {
          done(new Error("Not found"));
          return;
        }
        done(null, user);
      })
      .catch(err => {
        done(new Error(err.message));
      });
  });
};
