const nodemailer = require('nodemailer');
require('dotenv').config();
const ejs = require('ejs');
const path = require('path');


const transporter = nodemailer.createTransport({
    port: 465,               // true for 465, false for other ports
    host: "smtp.gmail.com",
    auth: {
        user: 'enfant.eveil.mg@gmail.com',
        pass: 'enfant@@2021',
    },
    secure: true,
});
module.exports.send = (from, to, subject, text, html, file, cc) => new Promise((resolve, reject) => {
    const mailData = {
        from: from,
        to: to,
        subject: subject,
        text: text,
        html: html,
        attachments: file || null,
        cc: cc || null
    };

    transporter.sendMail(mailData, (error, info) => {
        if (error) {
            reject(error);
        } else {
            resolve(info);
        }
    });
});

module.exports.sendTemplate = (params, templateDir, from, to, subject, file, cc) => new Promise((resolve, reject) => {
    const pathDir = path.join(process.cwd(), 'views', templateDir);
    ejs.renderFile(`${pathDir}.ejs`, params, {}, (err, result) => {
        if (err) {
            reject(err);
        } else {
            const mailData = {
                from: from,
                to: to,
                subject: subject,
                text: subject,
                html: result,
                attachments: file || null,
                cc: cc || null
            };

            transporter.sendMail(mailData, (error, info) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(info);
                }
            });
        }
    })
})
