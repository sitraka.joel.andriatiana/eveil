const express = require('express');
const {isLoggedIn, setUser, hasAccess} = require("../services/middlewares");
const router = express.Router();


module.exports = function (io) {
    const authentification = require('./auth')(io);
    const api = require('./api')(io);
    router.get('/', isLoggedIn, setUser, (req, res) => {
        // return res.redirect('/module/2');
        return res.render('index', {success: req.flash('success')});
    });
    router.get('/module/:id', isLoggedIn, setUser, hasAccess, (req, res) => {
        if (req.params.id == 2) {
            return res.render('Modules/module2', {success: req.flash('success')});
        } else if (req.params.id == 3) {
            return res.render('Modules/module3', {success: req.flash('success')});
        }
    });
    router.use('/', authentification);
    router.use('/api', api);

    return router;
};
