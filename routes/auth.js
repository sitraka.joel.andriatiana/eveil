const express = require('express');
const router = express.Router();
const passport = require('passport');
const moment = require('moment');


const {isLoggedIn} = require('../services/middlewares');
const {login, logout} = require('../controllers/authController');
const {changePassword, resetPassword} = require('../controllers/userController');


module.exports = (io) => {
    // Authentificaton TPE

    router.get('/auth', isLoggedIn, function (req, res) {
        res.json(res.locals.user);
    });

    router.get('/logout', isLoggedIn, logout);

    router.get('/login', login);

    router.post('/login',
        passport.authenticate('local-login', {
            failureRedirect: '/login',
            failureFlash: "Login ou mot de passe incorrect!",
        }),
        async function (req, res) {
            let originalUrl = req.session.originalUrl;
            //console.log(originalUrl)
            let user = req.user.dataValues;
            delete user.password;
            req.session.userData = user;
            req.flash('success', `Dèrnière connexion: ${moment(req.user.dateDerniereConn).format('DD/MM/YYYY HH:mm')}`);
            if (originalUrl === "" || originalUrl === undefined) {
                res.redirect('/');
            } else {
                delete req.session.originalUrl;
                res.redirect(originalUrl);
            }
        },
    );
// Change password
    router.get('/resetPassword/:urlMdp', changePassword);
    router.post('/resetPassword/:urlMdp', changePassword);
// Reset password
    router.get('/resetPassword', resetPassword);
    router.post('/resetPassword', resetPassword);
    return router;
};
